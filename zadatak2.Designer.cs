namespace A6Z2kk
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.pokusaj = new System.Windows.Forms.Button();
            this.Close = new System.Windows.Forms.Button();
            this.unos = new System.Windows.Forms.TextBox();
            this.trazenaRijec = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.preostalo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // pokusaj
            // 
            this.pokusaj.Location = new System.Drawing.Point(184, 66);
            this.pokusaj.Name = "pokusaj";
            this.pokusaj.Size = new System.Drawing.Size(75, 23);
            this.pokusaj.TabIndex = 0;
            this.pokusaj.Text = "Pokušaj";
            this.pokusaj.UseVisualStyleBackColor = true;
            this.pokusaj.Click += new System.EventHandler(this.pokusaj_Click);
            // 
            // Close
            // 
            this.Close.Location = new System.Drawing.Point(594, 316);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(75, 23);
            this.Close.TabIndex = 1;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // unos
            // 
            this.unos.Location = new System.Drawing.Point(57, 66);
            this.unos.Name = "unos";
            this.unos.Size = new System.Drawing.Size(100, 20);
            this.unos.TabIndex = 2;
            // 
            // trazenaRijec
            // 
            this.trazenaRijec.Location = new System.Drawing.Point(159, 198);
            this.trazenaRijec.Name = "trazenaRijec";
            this.trazenaRijec.Size = new System.Drawing.Size(249, 20);
            this.trazenaRijec.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Unesi slovo (ne č, ć, đ, š, ž):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(162, 164);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Tražena riječ:";
            // 
            // preostalo
            // 
            this.preostalo.Location = new System.Drawing.Point(518, 198);
            this.preostalo.Name = "preostalo";
            this.preostalo.Size = new System.Drawing.Size(100, 20);
            this.preostalo.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(515, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Preostalo pokušaja:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 351);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.preostalo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.trazenaRijec);
            this.Controls.Add(this.unos);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.pokusaj);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button pokusaj;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.TextBox unos;
        private System.Windows.Forms.TextBox trazenaRijec;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox preostalo;
        private System.Windows.Forms.Label label3;
    }
}

