using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        double a, b;

        private void minus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(op1.Text, out a)) MessageBox.Show("Pogresan unos operanda 1!", "Pogreška!");
            else if (!double.TryParse(op2.Text, out b)) MessageBox.Show("Pogresan unos operanda 2!", "Pogreška!");
            else Rminus.Text = (a - b).ToString();

        }

        private void puta_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(op1.Text, out a)) MessageBox.Show("Pogresan unos operanda 1!", "Pogreška!");
            else if (!double.TryParse(op2.Text, out b)) MessageBox.Show("Pogresan unos operanda 2!", "Pogreška!");
            else Rputa.Text = (a * b).ToString();
        }

        private void podijeljeno_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(op1.Text, out a)) MessageBox.Show("Pogresan unos operanda 1!", "Pogreška!");
            else if (!double.TryParse(op2.Text, out b)) MessageBox.Show("Pogresan unos operanda 2!", "Pogreška!");
            else if (b == 0) MessageBox.Show("Ne može se dijelit s 0!", "Pogreška!");
            else Rpodijeljeno.Text = (a / b).ToString();
        }

        private void sqrt_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(op1.Text, out a)) MessageBox.Show("Pogresan unos operanda 1!", "Pogreška!");
            else if (!double.TryParse(op2.Text, out b)) MessageBox.Show("Pogresan unos operanda 2!", "Pogreška!");
            else
            {
                o1korijen.Text = (Math.Sqrt(a)).ToString();
                o2korijen.Text = Math.Sqrt(b).ToString();
            }
        }

        private void cos_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(op1.Text, out a)) MessageBox.Show("Pogresan unos operanda 1!", "Pogreška!");
            else if (!double.TryParse(op2.Text, out b)) MessageBox.Show("Pogresan unos operanda 2!", "Pogreška!");
            else
            {
                coso1.Text = Math.Cos((a / 180)*Math.PI).ToString("0.######");
                coso2.Text = Math.Cos((b /180)*Math.PI).ToString("0.######");
            }
        }

        private void sin_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(op1.Text, out a)) MessageBox.Show("Pogresan unos operanda 1!", "Pogreška!");
            else if (!double.TryParse(op2.Text, out b)) MessageBox.Show("Pogresan unos operanda 2!", "Pogreška!");
            else
            {
                sino1.Text = Math.Sin((a / 180)*Math.PI).ToString("0.######");
                sino2.Text = Math.Sin((b / 180)*Math.PI).ToString("0.######");
            }
        }

        private void pow_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(op1.Text, out a)) MessageBox.Show("Pogresan unos operanda 1!", "Pogreška!");
            else if (!double.TryParse(op2.Text, out b)) MessageBox.Show("Pogresan unos operanda 2!", "Pogreška!");
            else 
            {
                o1nao2.Text = Math.Pow(a, b).ToString();
                o2nao1.Text = Math.Pow(b, a).ToString();
            
            }
        }

        private void log_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(op1.Text, out a)) MessageBox.Show("Pogresan unos operanda 1!", "Pogreška!");
            else if (!double.TryParse(op2.Text, out b)) MessageBox.Show("Pogresan unos operanda 2!", "Pogreška!");
            else if(a==0||b==0) MessageBox.Show("Ne postoji log0!", "Pogreška!");
            else 
            {
                logo1.Text = Math.Log10(a).ToString();
                logo2.Text = Math.Log10(b).ToString();
            }
        }

        private void close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void plus_Click(object sender, EventArgs e)
        {
            if(!double.TryParse(op1.Text, out a)) MessageBox.Show("Pogresan unos operanda 1!", "Pogreška!");
            else if(!double.TryParse(op2.Text, out b)) MessageBox.Show("Pogresan unos operanda 2!", "Pogreška!");
            else Rplus.Text = (a + b).ToString();
        }
    }
}