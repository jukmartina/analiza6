using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace A6Z2kk
{
    public partial class Form1 : Form
    {
        Random rand = new Random();
        List<string> lista = new List<string>();
        string path = "‪C:\\vjesala.txt";
        int brPokusaja;
        string trazena, rijec;
        char slovo;

        public Form1()
        {
            InitializeComponent();
        }

        public void Reset()
        {
            brPokusaja = 8;
            trazena = lista[rand.Next(lista.Count)];
            rijec = new string('*', rijec.Length);
            trazenaRijec.Text = rijec;
            preostalo.Text = brPokusaja.ToString();
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Reset();
            Application.Exit();
        }

        private void pokusaj_Click(object sender, EventArgs e)
        {
            int n = 0;
            slovo = unos.Text.ToUpper()[0];
            for (int i = 0; i < trazena.Length; i++)
            {
                if (slovo == trazena[i])
                {
                    rijec.Remove(i).Insert(i, slovo.ToString());
                    n++;
                }
            }
            if (n == 0) brPokusaja--;
            if (brPokusaja == 0)
            {
                MessageBox.Show("Nema više pokušaja!", "Kraj igre!");
                Reset();
            }
            else if (rijec == trazena)
            {
                trazenaRijec.Text = rijec;
                MessageBox.Show("Riječ pronađena!!", "Bravo!");
                Reset();
            }
            else
            {
                trazenaRijec.Text = rijec;
                preostalo.Text = brPokusaja.ToString();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null) // čitanje svih linija iz dat.
                {
                    lista.Add(line); // umetanje objekta u listu
                }
            }
        }



    }
    }
