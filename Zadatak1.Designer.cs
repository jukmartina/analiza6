namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.puta = new System.Windows.Forms.Button();
            this.podijeljeno = new System.Windows.Forms.Button();
            this.sqrt = new System.Windows.Forms.Button();
            this.cos = new System.Windows.Forms.Button();
            this.sin = new System.Windows.Forms.Button();
            this.pow = new System.Windows.Forms.Button();
            this.log = new System.Windows.Forms.Button();
            this.op1 = new System.Windows.Forms.TextBox();
            this.op2 = new System.Windows.Forms.TextBox();
            this.Rminus = new System.Windows.Forms.TextBox();
            this.Rplus = new System.Windows.Forms.TextBox();
            this.Rputa = new System.Windows.Forms.TextBox();
            this.Rpodijeljeno = new System.Windows.Forms.TextBox();
            this.o2korijen = new System.Windows.Forms.TextBox();
            this.o1korijen = new System.Windows.Forms.TextBox();
            this.coso2 = new System.Windows.Forms.TextBox();
            this.coso1 = new System.Windows.Forms.TextBox();
            this.sino2 = new System.Windows.Forms.TextBox();
            this.sino1 = new System.Windows.Forms.TextBox();
            this.o2nao1 = new System.Windows.Forms.TextBox();
            this.o1nao2 = new System.Windows.Forms.TextBox();
            this.logo2 = new System.Windows.Forms.TextBox();
            this.logo1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.korijenO1 = new System.Windows.Forms.Label();
            this.korijenO2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(156, 12);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(75, 23);
            this.plus.TabIndex = 0;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(156, 60);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(75, 23);
            this.minus.TabIndex = 1;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // puta
            // 
            this.puta.Location = new System.Drawing.Point(156, 113);
            this.puta.Name = "puta";
            this.puta.Size = new System.Drawing.Size(75, 23);
            this.puta.TabIndex = 2;
            this.puta.Text = "*";
            this.puta.UseVisualStyleBackColor = true;
            this.puta.Click += new System.EventHandler(this.puta_Click);
            // 
            // podijeljeno
            // 
            this.podijeljeno.Location = new System.Drawing.Point(156, 167);
            this.podijeljeno.Name = "podijeljeno";
            this.podijeljeno.Size = new System.Drawing.Size(75, 23);
            this.podijeljeno.TabIndex = 3;
            this.podijeljeno.Text = "/";
            this.podijeljeno.UseVisualStyleBackColor = true;
            this.podijeljeno.Click += new System.EventHandler(this.podijeljeno_Click);
            // 
            // sqrt
            // 
            this.sqrt.Location = new System.Drawing.Point(357, 12);
            this.sqrt.Name = "sqrt";
            this.sqrt.Size = new System.Drawing.Size(75, 23);
            this.sqrt.TabIndex = 4;
            this.sqrt.Text = "sqrt";
            this.sqrt.UseVisualStyleBackColor = true;
            this.sqrt.Click += new System.EventHandler(this.sqrt_Click);
            // 
            // cos
            // 
            this.cos.Location = new System.Drawing.Point(493, 14);
            this.cos.Name = "cos";
            this.cos.Size = new System.Drawing.Size(75, 23);
            this.cos.TabIndex = 5;
            this.cos.Text = "cos";
            this.cos.UseVisualStyleBackColor = true;
            this.cos.Click += new System.EventHandler(this.cos_Click);
            // 
            // sin
            // 
            this.sin.Location = new System.Drawing.Point(629, 14);
            this.sin.Name = "sin";
            this.sin.Size = new System.Drawing.Size(75, 23);
            this.sin.TabIndex = 6;
            this.sin.Text = "sin";
            this.sin.UseVisualStyleBackColor = true;
            this.sin.Click += new System.EventHandler(this.sin_Click);
            // 
            // pow
            // 
            this.pow.Location = new System.Drawing.Point(764, 14);
            this.pow.Name = "pow";
            this.pow.Size = new System.Drawing.Size(75, 23);
            this.pow.TabIndex = 7;
            this.pow.Text = "pow";
            this.pow.UseVisualStyleBackColor = true;
            this.pow.Click += new System.EventHandler(this.pow_Click);
            // 
            // log
            // 
            this.log.Location = new System.Drawing.Point(907, 14);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(75, 23);
            this.log.TabIndex = 8;
            this.log.Text = "log";
            this.log.UseVisualStyleBackColor = true;
            this.log.Click += new System.EventHandler(this.log_Click);
            // 
            // op1
            // 
            this.op1.Location = new System.Drawing.Point(23, 60);
            this.op1.Name = "op1";
            this.op1.Size = new System.Drawing.Size(100, 20);
            this.op1.TabIndex = 9;
            // 
            // op2
            // 
            this.op2.Location = new System.Drawing.Point(23, 167);
            this.op2.Name = "op2";
            this.op2.Size = new System.Drawing.Size(100, 20);
            this.op2.TabIndex = 10;
            // 
            // Rminus
            // 
            this.Rminus.Location = new System.Drawing.Point(237, 63);
            this.Rminus.Name = "Rminus";
            this.Rminus.Size = new System.Drawing.Size(100, 20);
            this.Rminus.TabIndex = 11;
            // 
            // Rplus
            // 
            this.Rplus.Location = new System.Drawing.Point(237, 14);
            this.Rplus.Name = "Rplus";
            this.Rplus.Size = new System.Drawing.Size(100, 20);
            this.Rplus.TabIndex = 12;
            // 
            // Rputa
            // 
            this.Rputa.Location = new System.Drawing.Point(237, 115);
            this.Rputa.Name = "Rputa";
            this.Rputa.Size = new System.Drawing.Size(100, 20);
            this.Rputa.TabIndex = 13;
            // 
            // Rpodijeljeno
            // 
            this.Rpodijeljeno.Location = new System.Drawing.Point(237, 170);
            this.Rpodijeljeno.Name = "Rpodijeljeno";
            this.Rpodijeljeno.Size = new System.Drawing.Size(100, 20);
            this.Rpodijeljeno.TabIndex = 14;
            // 
            // o2korijen
            // 
            this.o2korijen.Location = new System.Drawing.Point(357, 170);
            this.o2korijen.Name = "o2korijen";
            this.o2korijen.Size = new System.Drawing.Size(100, 20);
            this.o2korijen.TabIndex = 15;
            // 
            // o1korijen
            // 
            this.o1korijen.Location = new System.Drawing.Point(357, 87);
            this.o1korijen.Name = "o1korijen";
            this.o1korijen.Size = new System.Drawing.Size(100, 20);
            this.o1korijen.TabIndex = 16;
            // 
            // coso2
            // 
            this.coso2.Location = new System.Drawing.Point(493, 167);
            this.coso2.Name = "coso2";
            this.coso2.Size = new System.Drawing.Size(100, 20);
            this.coso2.TabIndex = 17;
            // 
            // coso1
            // 
            this.coso1.Location = new System.Drawing.Point(493, 87);
            this.coso1.Name = "coso1";
            this.coso1.Size = new System.Drawing.Size(100, 20);
            this.coso1.TabIndex = 18;
            // 
            // sino2
            // 
            this.sino2.Location = new System.Drawing.Point(629, 170);
            this.sino2.Name = "sino2";
            this.sino2.Size = new System.Drawing.Size(100, 20);
            this.sino2.TabIndex = 19;
            // 
            // sino1
            // 
            this.sino1.Location = new System.Drawing.Point(629, 87);
            this.sino1.Name = "sino1";
            this.sino1.Size = new System.Drawing.Size(100, 20);
            this.sino1.TabIndex = 20;
            // 
            // o2nao1
            // 
            this.o2nao1.Location = new System.Drawing.Point(764, 169);
            this.o2nao1.Name = "o2nao1";
            this.o2nao1.Size = new System.Drawing.Size(100, 20);
            this.o2nao1.TabIndex = 21;
            // 
            // o1nao2
            // 
            this.o1nao2.Location = new System.Drawing.Point(764, 87);
            this.o1nao2.Name = "o1nao2";
            this.o1nao2.Size = new System.Drawing.Size(100, 20);
            this.o1nao2.TabIndex = 22;
            // 
            // logo2
            // 
            this.logo2.Location = new System.Drawing.Point(907, 167);
            this.logo2.Name = "logo2";
            this.logo2.Size = new System.Drawing.Size(100, 20);
            this.logo2.TabIndex = 23;
            // 
            // logo1
            // 
            this.logo1.Location = new System.Drawing.Point(907, 87);
            this.logo1.Name = "logo1";
            this.logo1.Size = new System.Drawing.Size(100, 20);
            this.logo1.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Operand1:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Operand2:";
            // 
            // korijenO1
            // 
            this.korijenO1.AutoSize = true;
            this.korijenO1.Location = new System.Drawing.Point(354, 60);
            this.korijenO1.Name = "korijenO1";
            this.korijenO1.Size = new System.Drawing.Size(25, 13);
            this.korijenO1.TabIndex = 27;
            this.korijenO1.Text = "√o1";
            // 
            // korijenO2
            // 
            this.korijenO2.AutoSize = true;
            this.korijenO2.Location = new System.Drawing.Point(354, 142);
            this.korijenO2.Name = "korijenO2";
            this.korijenO2.Size = new System.Drawing.Size(25, 13);
            this.korijenO2.TabIndex = 28;
            this.korijenO2.Text = "√o2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(490, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "cos(o1)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(490, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "cos(o2)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(626, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "sin(o1)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(626, 142);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "sin(o2)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(761, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "o1ˇo2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(761, 142);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "o2ˇo1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(904, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 13);
            this.label11.TabIndex = 35;
            this.label11.Text = "logo1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(904, 142);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 36;
            this.label12.Text = "logo2";
            // 
            // close
            // 
            this.close.Location = new System.Drawing.Point(1018, 280);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(75, 23);
            this.close.TabIndex = 37;
            this.close.Text = "Close";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.close_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1092, 304);
            this.Controls.Add(this.close);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.korijenO2);
            this.Controls.Add(this.korijenO1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.logo1);
            this.Controls.Add(this.logo2);
            this.Controls.Add(this.o1nao2);
            this.Controls.Add(this.o2nao1);
            this.Controls.Add(this.sino1);
            this.Controls.Add(this.sino2);
            this.Controls.Add(this.coso1);
            this.Controls.Add(this.coso2);
            this.Controls.Add(this.o1korijen);
            this.Controls.Add(this.o2korijen);
            this.Controls.Add(this.Rpodijeljeno);
            this.Controls.Add(this.Rputa);
            this.Controls.Add(this.Rplus);
            this.Controls.Add(this.Rminus);
            this.Controls.Add(this.op2);
            this.Controls.Add(this.op1);
            this.Controls.Add(this.log);
            this.Controls.Add(this.pow);
            this.Controls.Add(this.sin);
            this.Controls.Add(this.cos);
            this.Controls.Add(this.sqrt);
            this.Controls.Add(this.podijeljeno);
            this.Controls.Add(this.puta);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.plus);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button puta;
        private System.Windows.Forms.Button podijeljeno;
        private System.Windows.Forms.Button sqrt;
        private System.Windows.Forms.Button cos;
        private System.Windows.Forms.Button sin;
        private System.Windows.Forms.Button pow;
        private System.Windows.Forms.Button log;
        private System.Windows.Forms.TextBox op1;
        private System.Windows.Forms.TextBox op2;
        private System.Windows.Forms.TextBox Rminus;
        private System.Windows.Forms.TextBox Rplus;
        private System.Windows.Forms.TextBox Rputa;
        private System.Windows.Forms.TextBox Rpodijeljeno;
        private System.Windows.Forms.TextBox o2korijen;
        private System.Windows.Forms.TextBox o1korijen;
        private System.Windows.Forms.TextBox coso2;
        private System.Windows.Forms.TextBox coso1;
        private System.Windows.Forms.TextBox sino2;
        private System.Windows.Forms.TextBox sino1;
        private System.Windows.Forms.TextBox o2nao1;
        private System.Windows.Forms.TextBox o1nao2;
        private System.Windows.Forms.TextBox logo2;
        private System.Windows.Forms.TextBox logo1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label korijenO1;
        private System.Windows.Forms.Label korijenO2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button close;
    }
}